package com.android.traceview;

import java.util.ArrayList;

public class TreePrint{
	private ArrayList<Call> mCallList;
	private ArrayList<Call> childList;
	private ArrayList<TreePrint> children;
	int level = 1;
	Call data;
	
	public TreePrint(ArrayList<Call> mCallList){
		this.mCallList = new ArrayList<Call>(mCallList);
		printTree(null);
		System.exit(0);
	}
	
	public void printTree(Call parent){
		while(mCallList.size() >0){
			//System.out.println(parent);
			for(int i = 0; i < mCallList.size(); i++){
				if(mCallList.get(i).mCaller == parent || mCallList.get(i).mCaller == null){
					System.out.println("(" + level+")"+mCallList.get(i).mName);
					++level;
					printTree(mCallList.remove(i));
					--level;
					i = 0;
				}
			}
		}
	}
	
	/*public TreePrint(Call data){
		this.data = data;
		children = new ArrayList<TreePrint>();
	}
	
	public void addChild(TreePrint child){
		children.add(child);
	}*/
	
	
}